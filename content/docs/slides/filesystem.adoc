---
title: Using the filesysten
weight: 4
description: The primary way shell scripts interact with the "outside world"
---
:source-highlighter: highlight.js

== Filesystem

When a program quits, the only thing that survives is the changes it made to your files. Everything else is wiped out. This (mostly) applies to bash too.

== Why does this matter?

Shell scripts usually call programs written in other languages.

Instead of passing variables, or command line args (which might get annoying if there are a lot), we might read and write to files instead. This lets us communicate large amounts of data quickly and in a formatted way (example: json).

== Everything is A File

Lets us easily compose programs with others without teaching them to communicate. Part of the "unix philosophy"

== The Filesystem

image::filesystem.png[]

=== As a Tree

Thinking of the fs as a tree is a good idea. There are a couple of caveats (links, volumes)

* Internal nodes are folders
* Leaves are files / or empty folders

=== Links

Need to think in advance which one you need.

* Hard links refer to the underlying file, not the "path" of the file.
** Delete or move the original file, this is fine
* Soft links refer to a path
** Delete or move the original file, this becomes useless

=== Volumes

You may mount extra hard drives, or partition your existing hard drive. Maybe you'll mount grace so you can pretend to work locally (`sshfs`).

These extra trees can point to each other.

Example: "/mnt/c" on wsl and "/Volumes" on macos

== Filetypes

* (regular) files
* folders
* sockets
** pipes / stdin
* links
* Special / block / device files

=== Sockets

* Used for networking, bidirectional communication
* Each one might represent a connection to another computer

image::socket.png[width=400]

===  Pipes

* Unidirectional Communication
* Used to redirect stdio (stdin, stdout)
* Can be named, in which case you need to know the name before using

== Permissions

Every file has the following five pieces of metadata associate with it

* Owner
* Group
* Read (r)
* Write (w)
* Execute (x)

=== As Octal Integers

The last 3 fields (read, write exec) dictate what one is allowed to with a file (besides creation/deletion). They are binary flags, and there is a separate copy of the flag for each of {owner, group, everyone}.

=== !

So we have 3 flags for 3 sets of people, giving as 9 bits of information. This will often be denoted as integers

====
* say rwx = 640
* r = 6 = 4 + 2
** You (4) and your group (2) can read
* w = 4: You can write to the file
* x = 0: no one is allowed to execute/run the file.
====

=== Common Permissions

* 644: you can write, everyone can read
* 600: only you can read/write
* 755: You can read/write, everyone can execute

=== For Directories

The permissions apply to folders too.

* Read: See a directory's contents
* Write: Add and remove files
* Exec: `cd` into the folder
** required to enter subfolders

=== Weird Permission Combos

* Directory with only write: you add files for others to read

* File with only write: You can only append to a file
** For recording sensitive info

* File/Program with only exec
** The program does stuff you're not allowed to do.
** You can run it, but not change its behavior

=== !

image::folder_perms_meme.jpeg[A meme about a difficult to access folder]
