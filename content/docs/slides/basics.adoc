---
title: Basics of shell programming
weight: 3
description: Features shells have in common with other languages
---
:source-highlighter: highlight.js

== Basics of shell programming

NOTE: The syntax differences are not the hard part.

Credit to https://ryanstutorials.net/bash-scripting-tutorial and the gnu bash docs for much of the material.

== Shebang `#!`

[%notitle]
=== Without a Shebang

Some scripts will be passed to their interpreter as is.

[source,console]
----
$ python pirate_music.py
----

However, this requires us to know which language a program was written, so we can call the right interpreter.

[%notitle]
=== Shebang Example

We can tell the operating system which interpreter to use with a shebang line.

Shebang Line:: The path to a program's interpreter should be written on the *first* line, preceded with `#!`

[source, console]
----
$ cat pirate_music.sh

#!/bin/bash

youtube-dl --format mp4 "https://..."
----

[%notitle]
=== `env`

WARNING: The location of an interpreter may not be known ahead of time. You can use `env` to help you find it.

[source, console]
----
$ cat pirate_music.sh

#!/usr/bin/env bash

youtube-dl --format mp4 "https://..."
----

== Command Execution

Call programs with a space separated list of arguments. The first "arg" is the program to be called. A nonzero exit status of the program will tell you if an error occured

[source, console]
----
$ prog1 arg1 arg2 argN ....
$ prog2 -s # short flag
$ prog3 --long-flag
$ prog4> # Enter your command here
----

When you read a man page or tutorial, you might see mandatory argument enclosed in <angle brackets> and optional ones in [square brackets]: +
`prog <arg1> [arg2 [arg3]] <arg3 [arg4]>`

== Variables

`=` is used to assign to and declare variables. However, due to space splitting, avoid spaces around the equals `=` sign. Otherwise it'll be interpreted as a command.

[source, bash, linenums]
----
my_variable="i like oranges"

other_var = "psych, this is a command"
----

=== Using variables

[source, bash, linenums]
----
echo "${var_in_quotes}"
----

We use {braces} to denote when the variable name ends

=== Misusing Variables

[source, bash, linenums]
----
echo "$var_in_quotes_mispelled" # <1>
echo $var_no_quotes # <2>
name="World" cmd -m "Hello $name" # <3>
----

<1> `_mispelled` get added to the variable name.
<2> If the variable has special characters or spaces, word splitting might occur (bad)
<3> Bash lets you temporarily set variables. Don't do this

Shellcheck will warn of these variable misuses, USE IT

=== Env Vars vs local/bash variables

Env variables can be thought of as global variables, they are handled by the OS (not the shell). Any program can read these. You create/update an env var with `export`: `export VAR=PASSWORD` (local vars can be added to ur env this way). These are used for settings. Example: `LSCOLORS`.

local variables are what we see just saw before, and can only be seen by the current shell session.

== Conditionals / If / Case

[source,bash,linenums]
----
if condition
then
    rm branch1
elif other_cond # <1>
then
    echo branch2
else # <1>
    cat branch3
fi
----
<1> `elif` and `else` are optional

=== Test, `[[`, `[`

The condition must be an invocation of some command. The exit status (0 or not) determines which branch is taken. There are no booleans here (though there is `true` and `false` commands)

Use `test` / `[` / `[[` to check conditions like string equality of whether a file exists. When test is invoked as `[` / `[[`, matching square brackets are required

[source,bash]
----
if [ -x ~/.zshrc ]; then
    echo found it # the file exists and is executable
else
    touch ~/.zshrc # otherwise create it
fi
----

== Loops

=== While

[source,bash,linenums]
----
while $cond
do
    $command
done
----

Loops support `break` and `continue`. There are also `until`, which inverts the while loop  condition.

=== For

Splitting occurs either on spaces or on `$IFS`

[source,bash,linenums]
----
IFS=','
fruits='apple,pear,banana'

for fruit in $fruits
do
    echo $fruit
done
----

Bash also supports ranges of integers like python: `{$start..$stop..$step}`, start and end are inclusive, step is optional.

== Functions

You need the `function` keyword and/or parenthesis `()`. However, arguments are *not* listed in the parenthesis

[source,bash,linenums]
----
print_something () {
    echo Hello I am a function
}
function print_something () { }
function print_something { }

print_something
----

=== Arguments

Functions are called like other programs (no parenthesis). Arguments are refererred to by integer index: `"${1}"`,`"${3}"`, `"${10}"`, etc.

[source,bash,linenums]
----
function ten () { echo "${10}"; }
ten 1 2 3 4 5 6 7 8 9 0
----

This prints `0`.
