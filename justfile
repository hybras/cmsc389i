build: slides
    hugo

# Uses fd-find. https://github.com/sharkdp/fd

find_slides:
    fd -e adoc . content/docs/slides --exclude=_index.adoc --exact-depth=1

slides:
    fd -e adoc . content/docs/slides --exclude=_index.adoc --exact-depth=1 \
        --exec just build_slide {} \;

build_slide slide:
    asciidoctor-revealjs \
    -a revealjsdir=https://cdn.jsdelivr.net/npm/reveal.js@3.9.2 \
    -a skip-front-matter \
    -a revealjs_hash=true \
    -a imagesdir=/cmsc389i/docs/slides/$(basename -s .adoc "{{slide}}")/ \
    -D static/docs/slides "{{slide}}"

# Uses entr. https://eradman.com/entrproject/
watch_slides: slides
    fd -e adoc . content/docs/slides --exclude=_index.adoc --exact-depth=1 \
    | entr -cp just build_slide /_

clean_slides:
    rm -v ./static/docs/slides/*.html

pyserv: build
    ln -s public cmsc389i
    python3 -m http.server
